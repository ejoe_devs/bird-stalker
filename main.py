#!/usr/bin/env python3

import time
import os

import owncloud
import picamera
from gpiozero import MotionSensor

# try motion sensor
pir = MotionSensor(4)
pir.wait_for_motion()
print("Motion detected")
with picamera.PiCamera() as camera:
    camera.resolution = (320, 240)
    # Camera warm-up time
    time.sleep(2)
    camera.capture('/data/image.jpg')
print('Picture taken')
oc = owncloud.Client('http://192.168.2.90:8080')
oc.login(os.environ['OWNCLOUD_USER'], os.environ['OWNCLOUD_PASSWORD'])
try:
    oc.mkdir('testdir')
except owncloud.HTTPResponseError:
    print('The directory already exists.')
oc.put_file('testdir/image.jpg', '/data/image.jpg')
link_info = oc.share_file_with_link('testdir/image.jpg')
print("Here is your link: " + link_info.get_link())
