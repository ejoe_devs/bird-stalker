#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import RPi.GPIO as GPIO
import time
from picamera import PiCamera

# inicializamos la cámara
camera = PiCamera()
# ajustamos la rotación
camera.rotation = 270

# iniciamos el sensor de movimiento
pin = 14
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.IN)

# le damos tiempo al sensor para que empiece a funcionar
print ("Press Ctrl+C to exit")
time.sleep(2)
print ("Ready")

try:
	# bucle principal
    while True:
		# si llega una señal de entrada desde el sensor, guardamos una
		# captura de la cámara poniendo como nombre la fecha y hora actual
        if GPIO.input(pin) == True:
            print ("Motion detected")
            timestr = time.strftime("%Y%m%d-%H%M%S")
            camera.capture(timestr, format="jpeg")
            print ("Capture saved")
            time.sleep(10)
except KeyboardInterrupt:
	# para parar el programa pulsando Ctrl+C
    print ("Program cancelled")
finally:
	# para asegurarnos que se liberan los recursos utilizados
	camera.close()
	GPIO.cleanup
