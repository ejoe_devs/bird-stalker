import json

with open("response.json", "r") as file:
    data = json.load(file)
    print("Request message: " + data["message"])
    print("Result: " + str(data["result"]))
    print("Result message: " + data["result"][0]["message"])
    print("Prediction: " + str(data["result"][0]["prediction"]))
    for value in data["result"][0]["prediction"]:
        print(type(value["label"]))
        print(value["label"])
