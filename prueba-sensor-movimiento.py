#!/usr/bin/env python3

import os
from gpiozero import MotionSensor
import time

pir = MotionSensor(4)

print ("Press Ctrl+C to exit")
time.sleep(2)
print ("Ready")

try: 
    while True:
        pir.wait_for_motion()
        print ("Motion detected")
        time.sleep(5)
except KeyboardInterrupt:
    print ("Program cancelled")
