#!/usr/bin/env python
# -*- coding: utf-8 -*-

from google.cloud import storage
import time

client = storage.Client()
bucket_name = "project-snow-white.appspot.com"
file_name = "20190301-111648"
timestr = time.strftime("%Y%m%d")

bucket = client.get_bucket(bucket_name)
blob = bucket.blob("img/prueba/" + file_name + ".jpeg")

blob.upload_from_filename(file_name)

print ("File {} uploaded to {}".format(
    file_name,
    "img/" + timestr + "/"))
